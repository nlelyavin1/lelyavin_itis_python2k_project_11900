from datetime import date

from django.contrib import admin
from django import forms

from .models import User, HistoryPriceStockOrBondOrCurrency, Follower, Dividend, Coupon, BrokerAccount, \
    News, Bond, Stock, Currency, Comment, Favourite, CountStockOnAccount, CountBondOnAccount, CountCurrencyOnAccount, \
    Operation

admin.site.empty_value_display = '(None)'


class DecadeBornListFilter(admin.SimpleListFilter):
    title = 'decade born'

    parameter_name = 'decade'
    # 1900 - 2020 it is 12 decade
    start_decade = 0
    end_decade = 12
    # 1900
    century = 19
    form = forms.CharField(max_length=15)

    def lookups(self, request, model_admin):
        century_local = self.century
        result = []
        number_of_year = 0
        for i in range(self.start_decade, self.end_decade + 1):

            if i % 10 == 0 and i != 0:
                century_local += 1
                number_of_year = 0

            result.append((f'{century_local}{number_of_year}0s', f'in the {century_local}{number_of_year}0'))
            number_of_year += 1
        return (
            result
        )

    def queryset(self, request, queryset):
        number_of_year = 0
        for i in range(self.start_decade, self.end_decade + 1):
            if i % 10 == 0 and i != 0:
                self.century += 1
                number_of_year = 0
            if self.value() == f'{self.century}{number_of_year}0s':
                return queryset.filter(date_birthday__gte=date(int(f'{self.century}{number_of_year}0'), 1, 1),
                                       date_birthday__lte=date(int(f'{self.century}{number_of_year}9'), 12, 31))
            number_of_year += 1


class BaseAdmin(admin.ModelAdmin):
    readonly_fields = ['created_at', 'updated_at']
    list_display = ['created_at', 'updated_at']
    fieldsets = [('Date', {'classes': ('collapse',),
                           'fields': [('created_at', 'updated_at')]})]
    list_max_show_all = 200
    list_per_page = 50

    class Meta:
        abstract = True


class DividendInline(admin.TabularInline):
    model = Dividend
    extra = 1
    max_num = 15


class FollowerInline(admin.TabularInline):
    model = Follower
    fk_name = 'user'
    extra = 1


class StocksInline(admin.TabularInline):
    model = CountStockOnAccount
    extra = 1


class BondsInline(admin.TabularInline):
    model = CountBondOnAccount
    extra = 1


class HistoryPriceStockOrBondOrCurrencyInline(admin.TabularInline):
    model = HistoryPriceStockOrBondOrCurrency
    extra = 1


class StockInline(admin.TabularInline):
    model = Stock
    extra = 1


class CurrencyInline(admin.TabularInline):
    model = Currency
    extra = 1


class CurrenciesInline(admin.TabularInline):
    model = CountCurrencyOnAccount
    extra = 1


class CouponsInline(admin.TabularInline):
    model = Coupon
    extra = 1
    max_num = 15


class OperationInline(admin.TabularInline):
    model = Operation
    extra = 1


@admin.register(User)
class UserAdmin(BaseAdmin):
    # Отображение на странице со списком юзеров
    list_display = ['username', 'role', 'count_followers', 'count_followings', 'date_birthday', 'profile_photo']
    list_display.extend(BaseAdmin.list_display)
    list_display_links = ['username', 'count_followers', 'count_followings', 'date_birthday', 'profile_photo']
    # Создает поиск по данным полям  TODO почему-то не указывает по каким
    search_fields = ['username', 'email', 'role']
    readonly_fields = ['new_user_status_check']
    readonly_fields.extend(BaseAdmin.readonly_fields)
    # Фильтры для вывода списка с ограничениями
    list_filter = [DecadeBornListFilter, 'is_superuser', 'username', ('profile_photo', admin.EmptyFieldListFilter)]

    inlines = [FollowerInline]
    # Непосредственно отображаемые поля юзера
    fieldsets = [
        ('Authorization Fields', {
            'fields': [('username', 'password')]}),
        ('Email', {'fields': ['email']}),
        ('Other', {'classes': ('collapse',),
                   'fields': [('count_followers', 'count_followings', ), 'profile_photo',
                              'new_user_status_check', 'date_birthday']}),
        ('Meta', {'classes': ('collapse',),
                  'fields': [('role', 'is_superuser',)]}),
    ]
    # Унаследованные поля
    fieldsets.extend(BaseAdmin.fieldsets)

    save_on_top = True
    list_editable = ['role']


@admin.register(BrokerAccount)
class BrokerAccountAdmin(BaseAdmin):
    readonly_fields = ['get_username']
    list_display = ['get_username']
    list_display.extend(BaseAdmin.list_display)

    inlines = [StocksInline, BondsInline, CurrenciesInline, OperationInline]
    fieldsets = [
        ('User', {
            'fields': ['user']}),
        ('Money', {'fields': [('dividend_yield', 'coupon_yield'), ('replenishment_of_money', 'withdrawn_of_money')]})

    ]
    search_fields = ['user__username', 'stock__ticker', 'stock__name', 'bond__ticker', 'bond__name',
                     'currency__ticker', 'currency__name']

    def get_username(self, obj):
        return obj.user.username

    get_username.short_description = 'User'


@admin.register(Operation)
class OperationAdmin(BaseAdmin):
    list_display = ['broker_account', 'stock', 'bond', 'currency']
    list_display.extend(BaseAdmin.list_display)
    fieldsets = [
        ('Main', {'fields': ['count_product', 'price_product', 'buy_or_sell']}),
    ]
    fieldsets.extend(BaseAdmin.fieldsets)


@admin.register(Follower)
class FollowerAdmin(BaseAdmin):
    list_display = ['get_username']
    list_display.extend(BaseAdmin.list_display)
    fieldsets = [
        ('Main', {'fields': ['user', 'follower']}),
    ]
    fieldsets.extend(BaseAdmin.fieldsets)

    def get_username(self, obj):
        return obj.user.username

    get_username.short_description = 'User'


@admin.register(Dividend)
class DividendAdmin(admin.ModelAdmin):
    list_display = ['stock', 'price', 'payment_date']


@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    list_display = ['ticker', 'name', 'price', 'currency']
    list_display.extend(BaseAdmin.list_display)

    inlines = [StocksInline, HistoryPriceStockOrBondOrCurrencyInline]

    fieldsets = [
        ('Main Info', {'fields': ['ticker', 'name', 'information']}),
        ('Price', {'fields': ['price', 'currency']})
    ]

    exclude = ['account']


@admin.register(Bond)
class BondAdmin(admin.ModelAdmin):
    list_display = ['ticker', 'name', 'price', 'currency']
    list_display.extend(BaseAdmin.list_display)

    inlines = [BondsInline]

    fieldsets = [
        ('Main Info', {'fields': ['ticker', 'name', 'information']}),
        ('Money', {'fields': [('price', 'percent_yield', 'currency')]}),
        ('Date', {'fields': ['date_maturity', ]}),
    ]

    exclude = ['account']


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ['ticker', 'name', 'price']
    list_display.extend(BaseAdmin.list_display)

    inlines = [HistoryPriceStockOrBondOrCurrencyInline]

    fieldsets = [
        ('Main Info', {'fields': ['ticker', 'name', 'information', 'symbol_currency']}),
        ('Money', {'fields': ['price', 'purchase_currencies']})
    ]

    exclude = ['account']


@admin.register(Coupon)
class CouponAdmin(admin.ModelAdmin):
    list_display = ['price', 'payment_date']
    inlines = []


@admin.register(News)
class NewsAdmin(BaseAdmin):
    list_display = ['news_title', 'news_url']
    list_display.extend(BaseAdmin.list_display)

    fieldsets = [
        ('Text', {'fields': ['news_title', 'news_text']}),
        ('Url', {'fields': ['news_url']}),
    ]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['get_username', 'like', 'text']

    def get_username(self, obj):
        return obj.account.user.username

    get_username.short_description = 'User'


@admin.register(HistoryPriceStockOrBondOrCurrency)
class HistoryPriceStockOrBondOrCurrencyAdmin(admin.ModelAdmin):
    list_display = ['stock', 'bond', 'currency']

    fieldsets = [
        ('Company', {'fields': [('stock', 'bond', 'currency')]}),
        ('Money', {'fields': [('price', 'date_of_price')]}),
    ]


@admin.register(Favourite)
class FavouriteAdmin(admin.ModelAdmin):
    list_display = ['account', 'stock', 'bond', 'currency']
    fields = ['account', ('stock', 'bond', 'currency')]
