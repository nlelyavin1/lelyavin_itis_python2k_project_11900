from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView
from pathlib import Path

from .investments_services import CheckLoginView, InvestmentInstrumentsOfBrokerAccount, CheckBrokerAccountView, \
    get_user_on_username_or_none, get_stock_on_ticker, get_bond_on_ticker, get_currency_on_ticker, user_register, \
    get_operations_on_user_or_none, get_comments_on_id_or_none, get_user_and_brokeraccount_on_username_or_none, \
    update_profile
from .forms import RegistrationForm, LoginForm, UserUpdateProfileForm
from .models import Stock, Bond, Currency, BrokerAccount


def main(request):
    return render(request, 'investments/main.html')


def registration(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = user_register(form)
            login(request, user)
            return redirect('main')

    return render(request, 'investments/registration.html', {
        'form': form
    })


def investments_product(request):
    return render(request, 'investments/investments_product.html')


class StocksView(ListView):
    model = Stock
    template_name = 'investments/stocks.html'

    queryset = Stock.objects.all().prefetch_related('comment_set').annotate(count_comments=Count('comment')).order_by(
        'name')[:6]


class BondsView(ListView):
    model = Bond
    template_name = 'investments/bonds.html'
    queryset = Bond.objects.all().prefetch_related('comment_set').annotate(count_comments=Count('comment')).order_by(
        'name')[:6]


class CurrenciesView(ListView):
    model = Currency
    template_name = 'investments/currencies.html'
    queryset = Currency.objects.all().prefetch_related('comment_set').\
        annotate(count_comments=Count('comment')).order_by('name')[:6]


class UserBrokerAccountView(LoginRequiredMixin, CheckBrokerAccountView):
    def get(self, request, *args, **kwargs):
        investment_instruments = InvestmentInstrumentsOfBrokerAccount(request.user)
        count_stocks = investment_instruments.stocks
        count_bonds = investment_instruments.bonds
        count_currencies = investment_instruments.currencies
        broker_account = investment_instruments.broker_account
        cost_of_account = investment_instruments.cost_of_account()

        context = {
            'count_stocks': count_stocks, 'count_bonds': count_bonds, 'count_currencies': count_currencies,
            'broker_account': broker_account, 'cost_of_account': cost_of_account
        }
        return render(request, 'investments/broker_account.html',
                      context)

    # def post(self, request, *args, **kwargs):
    #     return render(request, 'investments/login.html', {})


class UserProfileView(LoginRequiredMixin, View):
    # form_user_fields = UserProfileForm

    def get(self, request, pk, *args, **kwargs):

        print('bs_0' + str(1))
        user = get_user_and_brokeraccount_on_username_or_none(username=pk)
        operations = get_operations_on_user_or_none(user)
        context = {'user_profile': user, 'operations': operations}
        return render(request, 'investments/user_profile.html', context)


class UserUpdateProfileView(LoginRequiredMixin, View):

    def get(self, request, pk, *args, **kwargs):
        form = UserUpdateProfileForm
        context = {'user_form': form}
        return render(request, 'investments/update_user_profile.html', context)

    def post(self, request, *args, **kwargs):
        form = UserUpdateProfileForm(request.POST or None, request.FILES)
        if form.is_valid():
            update_profile(request.user, form)
        return redirect(f'/user-profile/{request.user}')


class StockView(LoginRequiredMixin, CheckBrokerAccountView):

    def get(self, request, ticker, *args, **kwargs):
        stock = get_stock_on_ticker(ticker)
        comments = get_comments_on_id_or_none(request.user.brokeraccount.id, stock_id=stock.id)
        context = {'stock': stock}
        if comments:
            context['comments'] = comments

        return render(request, 'investments/stock.html', context)

    def post(self, request, *args, **kwargs):
        pass


class BondView(LoginRequiredMixin, CheckBrokerAccountView):

    def get(self, request, ticker, *args, **kwargs):
        bond = get_bond_on_ticker(ticker=ticker)
        comments = get_comments_on_id_or_none(request.user.brokeraccount.id, bond_id=bond.id)
        context = {'bond': bond}

        if comments:
            context['comments'] = comments

        return render(request, 'investments/bond.html', context)

    def post(self, request, ticker, *args, **kwargs):
        bond = get_bond_on_ticker(ticker=ticker)
        return redirect('main')


class CurrencyView(LoginRequiredMixin, CheckBrokerAccountView):

    def get(self, request, ticker, *args, **kwargs):
        currency = get_currency_on_ticker(ticker=ticker)
        comments = get_comments_on_id_or_none(request.user.brokeraccount.id, currency_id=currency.id)
        context = {'currency': currency}

        if comments:
            context['comments'] = comments
        print(context)
        return render(request, 'investments/currency.html', context)


class MyLoginView(CheckLoginView):
    form = LoginForm

    def get(self, request, *args, **kwargs):
        return render(request, 'investments/login.html', {'form': self.form})

    def post(self, request, *args, **kwargs):
        self.form = LoginForm(request.POST)

        if self.form.is_valid():
            username = self.form.cleaned_data['username']
            password = self.form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is None:
                self.form.add_error('username', 'Неправильный логин или пароль')
            else:
                login(request, user)
                return redirect('main')

        return render(request, 'investments/login.html', {'form': self.form})


@login_required
def logout_view(request):
    logout(request)
    return redirect('main')
