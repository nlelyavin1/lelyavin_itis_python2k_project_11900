from django.contrib.auth.forms import UserCreationForm

from .models import User

from django.forms import ModelForm
from django import forms


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = (
             'username', 'email',
        )

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['password1'] != cleaned_data['password2']:
            self.add_error('password1', 'Пароли не совпадают')
        return cleaned_data


class LoginForm(forms.Form):
    username = forms.CharField(label='Логин')
    password = forms.CharField(label='Пароль')


class UserProfileForm(ModelForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'date_birthday', 'profile_photo', 'count_followers', 'count_followings')


class UserUpdateProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ('profile_photo',)
