import os

from django.db.models import Count, Q
from django.shortcuts import redirect
from django.views import View
from .models import BrokerAccount, User, Stock, Bond, Currency, Operation, Comment


class CheckLoginView(View):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('main')

        return super().dispatch(request, *args, **kwargs)


def get_broker_account_on_user_or_none(user):
    try:
        print(BrokerAccount.objects.get(user=user))
        return BrokerAccount.objects.get(user=user)
    except BrokerAccount.DoesNotExist:
        return None


def update_profile(user, form):
    user = get_user_on_username_or_none(user.username)
    profile_photo = form.cleaned_data['profile_photo']
    url = f'/media/user_{user.id}/{profile_photo}'
    print(url)
    if os.path.exists(url):
        print(f'uploads / {url}')
        user.profile_photo.path = f'uploads / {url}'
    else:
        user.profile_photo = profile_photo

    user.save()


def get_broker_account_and_products_on_user_or_none(user):
    try:
        return BrokerAccount.objects.prefetch_related('countstockonaccount_set'). \
            prefetch_related('countbondonaccount_set'). \
            prefetch_related('countcurrencyonaccount_set'). \
            get(user=user)
    except BrokerAccount.DoesNotExist:
        return None


def get_user_on_username_or_none(username):
    try:
        return User.objects.get(username=username)
    except User.DoesNotExist:
        return None


def get_user_and_brokeraccount_on_username_or_none(username):
    try:
        return User.objects.select_related('brokeraccount').get(username=username)
    except User.DoesNotExist:
        return None


def get_stock_on_ticker(ticker):
    return Stock.objects.get(ticker=ticker)


def get_bond_on_ticker(ticker):
    return Bond.objects.get(ticker=ticker)


def get_currency_on_ticker(ticker):
    return Currency.objects.get(ticker=ticker)


def get_comments_on_id_or_none(account_id, stock_id=None, bond_id=None, currency_id=None):
    try:
        return Comment.objects.filter(account_id=account_id, stock_id=stock_id, bond_id=bond_id,
                                      currency_id=currency_id). \
            values_list('text', 'account_id', 'created_at', 'updated_at'). \
            order_by('created_at')
    except Comment.DoesNotExist:
        return None


def get_operations_on_user_or_none(user):
    try:
        return Operation.objects.filter(broker_account=user.brokeraccount).order_by('created_at')[:5][::-1]
    except Operation.DoesNotExist:
        return None


def user_register(form):
    """Регистрация"""
    email = form.cleaned_data['email']
    username = form.cleaned_data['username']
    password1 = form.cleaned_data['password1']
    user = User(email=email, username=username)
    user.set_password(password1)
    user.save()
    return user


class CheckBrokerAccountView(View):
    """Проверка на то, что у бользователя есть БРОКЕРСКИЙ аккаунт"""

    def dispatch(self, request, *args, **kwargs):
        try:
            if not get_broker_account_on_user_or_none(user=request.user):
                return redirect('create_broker_account')
        except BrokerAccount.DoesNotExist:
            return redirect('create_broker_account')

        return super().dispatch(request, *args, **kwargs)


class InvestmentInstrumentsOfBrokerAccount:
    def __init__(self, user) -> None:
        self.broker_account = get_broker_account_and_products_on_user_or_none(user=user)

        self.stocks = self.broker_account.countstockonaccount_set.all()
        self.bonds = self.broker_account.countbondonaccount_set.all()
        self.currencies = self.broker_account.countcurrencyonaccount_set.all()

    def cost_of_account(self):
        cost = 0
        for stock in self.stocks:
            cost += stock.stock.price * stock.count_stock * stock.stock.currency.price

        for bond in self.bonds:
            cost += bond.bond.price * bond.count_bond * bond.bond.currency.price

        for currency in self.currencies:
            cost += currency.currency.price * currency.count_currency
        return cost
