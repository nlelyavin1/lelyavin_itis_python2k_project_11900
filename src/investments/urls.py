from django.urls import path, re_path, include
from .views import registration, main, MyLoginView, logout_view, UserBrokerAccountView, UserProfileView, \
    StockView, BondView, CurrencyView, investments_product, StocksView, BondsView, CurrenciesView, UserUpdateProfileView

urlpatterns = [
    path('registration', registration, name='registration'),
    path('', main, name='main'),
    path('login', MyLoginView.as_view(), name='login'),
    path('logout', logout_view, name='logout'),
    path('broker-account', UserBrokerAccountView.as_view(), name='account'),
    path('user-profile/<str:pk>', UserProfileView.as_view(), name='user_profile'),
    path('user-update/<str:pk>', UserUpdateProfileView.as_view(), name='update_user_profile'),
    path('investments-product', investments_product, name='investments_product'),
    path('investments-product/', include([
        re_path(r'^stocks/ticker-(?P<ticker>\w*)$', StockView.as_view(), name='stock'),
        path('bonds/ticker-<str:ticker>', BondView.as_view(), name='bond'),
        path('currency/ticker-<str:ticker>', CurrencyView.as_view(), name='currency'),
        path('stocks', StocksView.as_view(), name='stocks'),
        path('bonds', BondsView.as_view(), name='bonds'),
        path('currencies', CurrenciesView.as_view(), name='currencies'),

    ])),


]
