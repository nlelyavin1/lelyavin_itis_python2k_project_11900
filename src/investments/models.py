from datetime import datetime
import os
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager as DjangoUserManager
from django.db import models
from djchoices import DjangoChoices, ChoiceItem


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    def create_user(self, email, password=None, **kwargs):
        user = self.model(email=email)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password=None, **kwargs):
        user = self.model(username=username, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


class Role(DjangoChoices):
    admin = ChoiceItem()
    user = ChoiceItem()


def user_directory_path(instance, filename):
    return f'user_{instance.id}/{filename}'


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    username = models.CharField(unique=True, max_length=25, help_text='Maximum 25 characters', null=True)
    objects = UserManager()
    date_birthday = models.DateField(null=True)
    email = models.EmailField(unique=True)
    profile_photo = models.ImageField(upload_to=user_directory_path,
                                      null=True, blank=True)
    role = models.CharField(choices=Role.choices, default=Role.user, max_length=50)
    count_followers = models.IntegerField(verbose_name='Количество подписчиков', default=0)
    count_followings = models.IntegerField(verbose_name='Количество подписок', default=0)

    def new_user_status_check(self):
        if self.created_at.day - datetime.now().day <= 7:
            return True
        else:
            return False

    new_user_status_check.short_description = "Новый ли пользователь"

    @property
    def is_staff(self):
        return self.is_superuser

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return f'{self.username}'

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        ordering = ['role']


class Follower(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    follower = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Подписчик', null=True,
                                 related_name='follower_of_user')

    def __str__(self):
        return f'Подписчики пользователя {self.user.username}'


class BaseCouponAndDividend(models.Model):
    price = models.DecimalField(max_digits=14, decimal_places=7)
    payment_date = models.DateField()

    class Meta:
        abstract = True


class BrokerAccount(BaseModel):

    replenishment_of_money = models.DecimalField(verbose_name='Пополненений на сумму', max_digits=14, decimal_places=7,
                                                 null=True, blank=True)
    withdrawn_of_money = models.DecimalField(verbose_name='Выведено на сумму', max_digits=14, decimal_places=7,
                                             null=True, blank=True)
    dividend_yield = models.DecimalField(verbose_name='Дивидендный доход', max_digits=14, decimal_places=7,
                                         null=True, blank=True)
    coupon_yield = models.DecimalField(verbose_name='Купонный доход', max_digits=14, decimal_places=7,
                                       null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Владелец аккаунта')

    def __str__(self):
        return f'{self.user.username}'


class News(BaseModel):
    news_url = models.URLField(verbose_name='Ссылки на новости компании')
    news_title = models.CharField(max_length=100)
    news_text = models.TextField()

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'


class BaseStockAndBondAndCurrency(BaseModel):
    news = models.ManyToManyField(News)
    ticker = models.SlugField(unique=True, verbose_name='Тикер компании')
    price = models.DecimalField(max_digits=14, decimal_places=7, verbose_name='Цена на данный момент')
    name = models.CharField(max_length=100, verbose_name='Название компании')
    information = models.TextField(verbose_name='Информация о компании', null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self.ticker}'


class Currency(BaseStockAndBondAndCurrency):
    symbol_currency = models.CharField(max_length=1, verbose_name='Символ валюты', null=True, blank=True)
    # В этой валюте покупается валюты модели. Пример: если обьект USDRUB, то у него currency будет RUB.
    purchase_currencies = models.OneToOneField('self', on_delete=models.DO_NOTHING, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Currencies'


class Bond(BaseStockAndBondAndCurrency):
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING, null=True, blank=True,
                                 verbose_name='В этой валюте происходят покупки')
    date_maturity = models.DateField(verbose_name='Дата погашения облигации')
    percent_yield = models.DecimalField(max_digits=10, decimal_places=7, verbose_name='Процентная доходность')


class Stock(BaseStockAndBondAndCurrency):
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING, null=True, blank=True,
                                 verbose_name='В этой валюте происходят покупки')


class ForeignKeyOnBrokerAccount(models.Model):
    broker_account = models.ForeignKey(BrokerAccount, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class CountStockOnAccount(ForeignKeyOnBrokerAccount):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    count_stock = models.IntegerField(verbose_name='Количество акций')


class CountBondOnAccount(ForeignKeyOnBrokerAccount):
    bond = models.ForeignKey(Bond, on_delete=models.CASCADE)
    count_bond = models.IntegerField(verbose_name='Количество облигаций')


class CountCurrencyOnAccount(ForeignKeyOnBrokerAccount):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    count_currency = models.IntegerField(verbose_name='Количество валюты')


# TODO нужно изменить verbose_name
class Dividend(BaseCouponAndDividend):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE, null=True, blank=True)


class Coupon(BaseCouponAndDividend):
    bond = models.ForeignKey(Bond, on_delete=models.CASCADE)


class LikeDislike(DjangoChoices):
    like = ChoiceItem()
    dislike = ChoiceItem()


class StockBondCurrency(BaseModel):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE, null=True, blank=True)
    bond = models.ForeignKey(Bond, on_delete=models.CASCADE, null=True, blank=True)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        if self.stock:
            return f'{self.stock.ticker}'
        if self.bond:
            return f'{self.bond.ticker}'
        if self.currency:
            return f'{self.currency.ticker}'

    class Meta:
        abstract = True


class Comment(StockBondCurrency):
    account = models.ForeignKey(BrokerAccount, on_delete=models.DO_NOTHING)
    text = models.CharField(max_length=1000, verbose_name='Текст комментария')
    like = models.CharField(choices=LikeDislike.choices, null=True, default=None, max_length=10)


class HistoryPriceStockOrBondOrCurrency(StockBondCurrency):
    price = models.DecimalField(max_digits=14, decimal_places=7, verbose_name='Цена в определенный момент времени')
    date_of_price = models.DateTimeField(verbose_name='Дата по который будет строиться график')

    class Meta:
        verbose_name = 'History price stock or bond or currency'
        verbose_name_plural = 'History price stocks or bonds or currencies'


class Favourite(StockBondCurrency):
    account = models.ForeignKey(BrokerAccount, on_delete=models.CASCADE)


class BuyOrSell(DjangoChoices):
    buy = ChoiceItem()
    sell = ChoiceItem()


class Operation(StockBondCurrency):
    broker_account = models.ForeignKey(BrokerAccount, on_delete=models.DO_NOTHING)
    count_product = models.IntegerField(verbose_name='Количество')
    price_product = models.DecimalField(max_digits=14, decimal_places=7, verbose_name='Цена')
    buy_or_sell = models.CharField(choices=BuyOrSell.choices, max_length=4)
