from django.urls import include, path
from rest_framework import routers

from api import views
from api.views import main_api_view


router = routers.DefaultRouter()
router.register('users', views.UserViewSet, 'simple')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', main_api_view),
    *router.urls,
]

