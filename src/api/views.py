from django.http import Http404
from django.shortcuts import render
from rest_framework import viewsets, permissions, status
from rest_framework.decorators import api_view
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import UserSerializer
from investments.models import User


@api_view(['GET'])
def main_api_view(request):
    """Method for checking api"""
    return Response({'status': 'ok'})


class IsAuthenticated(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

