from rest_framework import serializers

from investments.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'is_superuser', 'date_birthday')
        read_only_fields = ('role',)
